Pandemic University Lecture Seat Booking System (PULSeBS)
===============================================

Version 2.0 - 2020-12-05

<style>
h5 {
  margin: 5px;
  font-family: sans-serif;
  font-size: 130%;
  margin-top: 3em;
  margin-bottom: 0;
}

h5 + p {
  font-family: sans-serif;
  font-size: 110%;
  padding: 2em;
  background-color: #FFEFD5;
  border: 2px solid black;
  border-radius: 5px;
  margin-top: 0;
  margin-bottom: 2em;
  transform: rotate(-4deg) translateY(-10%);
  width: 60%;
  min-width: 500pt;
}
</style>

Contents:

- [User stories](#toc_1)
- [User stories - 2nd part](#toc_27)
- [Jira import instructions](#toc_42)
- [Sample data](#toc_43)

----

## User Stories

The stories are number in order of priority.

##### Story 1
As a student I want to book a seat for one of my lectures so that I can attend it

- You need a login system. We will provide you more details in the future, for now use a simple login.
- In practice you can assume the the accounts are already set up
- A student can book only lectures for the courses he is enrolled in
 
##### Story 2
As a teacher I want to get notified of the number of students attending my next lecture so that I am informed

- Notification means an email is sent to the teacher.
- The notification can be sent once the deadline for booking (i.e. 23:00 of the day before the lecture) expires
- You can consider only one teacher per course
- The email should be real, you can use fake emails for the demo

##### Story 3
As a teacher I want to access the list of students booked for my lectures so that I am informed

- Access means you access a web page with the details.


##### Story 4
As a student I want to get an email confirmation of my booking so that I am informed
##### Story 5
As a student I want to cancel my booking so that I am free
##### Story 6
As a student I want to access a calendar with all my bookings for the upcoming weeks
##### Story 7
As a teacher I want to cancel a lecture up to 1h before its scheduled time
##### Story 8
As a student I want to get notified when a lecture is cancelled

- Notification must be sent via email

##### Story 9
As a teacher I want to turn a presence lecture into a distance one up to 30 mins before its scheduled time

##### Story 10
As a teacher I want to access the historical data about bookings so that I can plan better

- The teachers want to know the number of bookings for all the previous lectures, possibly organized in time. It would be nice to have different detail levels: per single lecture, per week (average), per month (average).
Also a graph with all past bookings would be appreciated.

##### Story 11
As a booking manager I want to monitor usage (booking, cancellations, attendance) of the system

##### Story 12
As a support officer I want to upload the list of students, courses, teachers, lectures, and classes to setup the system

- The system manages only in-presence lectures, that are therefore bookable.

##### Story 13
As a student I want to be put in a waiting list when no seats are available in the required lecture
##### Story 14
As a student in the waiting list I want to be added to the list of students booked when someone cancels their booking so that I can attend the lecture

- When a student cannot book a seat because the lecture is already fully booked, she'd like to receive an email she's picked from the waiting list (because someone else cancelled), the alternative would be to keep refreshing the page to see if one gets into the lecture.
  
  Of course when the student access to her page she can see the lecture as booked.

##### Story 15
As a student I want to get notified when I am taken from the waiting list so that I can attend the lecture
##### Story 16
As a booking manager I want to generate a contact tracing report starting with a positive student so that we comply with safety regulations
##### Story 17
As a support officer I want to update the list of bookable lectures

-  it is possible that at some point some restriction are introduced by the local or regional government, e.g. only lectures for the first year can be booked, following years are no more bookable until further notice


##### Story 18
As a teacher I want to record the students present at my lecture among those booked so that I can keep track of actual attendance
##### Story 19
As a teacher I want to access the historical data about presence so that I can assess the course
##### Story 20
As a support officer I want to modify the schedule of courses so that data is up-to-date

- when a modification is applied it is valido from a given date to all the future lectures. The previous lecture keep their date (since they have already been held and the history must be preserved)


##### Story 21
As a student I want to get a notification when the schedule for a lecture I booked for is changed so that I am informed
##### Story 22
As a student I want to access a tutorial for using the system so that I can use it properly
##### Story 23
As a teacher I want to access a tutorial for using the system so that I can use it correctly
##### Story 24
As a booking manager I want to generate a contact tracing report starting with a positive teacher so that we comply with safety regulations
##### Story 25
As a support officer I want to exclude holidays from bookings

- On holidays  days  no lectures takes place.

## User Stories (2^nd part)

##### Story 26
As a user I want to be authenticated using my softeng credentials.
##### Story 27
As a student I want to book my lectures through a Telegram bot
##### Story 28
As a student I want to get a summary of my bookings using a Telegram bot
##### Story 29
As a student I want to get informed when I get picked from the waiting list trough Telegram
##### Story 30
As a teacher I want to get a summary of my lectures bookings through a Telegram bot
##### Story 31
As a student I want to file a description of a malfunction (ticket) so that the issue can be fixed
##### Story 32
As a teacher I want to file a description of a malfunction (ticket) so that the issue can be fixed
##### Story 33
As a student I want to get notified about the handling and solution of a previously filed ticket so that I am informed
##### Story 34
As a teacher I want to get notified about the handling and solution of a previously filed ticket so that I am informed
##### Story 35
As a support officer I want to be able to access and work on tickets so that they can be solved
##### Story 36
As a booking manager I want to monitor the status of filed tickets
##### Story 37
As a support officer I want to manually fix an existing booking as part of a ticket handling so that I can solve a problem
##### Story 38
As a course coordinator I want to access booking statistics, overall and by course, college and year of degree
##### Story 39
As a booking manager I want to restrict bookings to a limited set of courses so that I can comply with changing safety limitations


----

## Jira Import

To import the stories into Jira without having to type them one by one, you can use the [CSV file](PULSeBS_Stories.csv) available and follow the [instructions on the Atlassian web site](https://support.atlassian.com/jira-cloud-administration/docs/import-data-from-a-csv-file/).


## Sample data

Here the Product Owner will post the data file that can be used for testing the system.

- [`Students.csv`](test/Students.csv)
- [`Professors.csv`](test/Professors.csv)
- [`Courses.csv`](test/Courses.csv)
- [`Enrollment.csv`](test/Enrollment.csv)
- [`Schedule.csv`](test/Schedule.csv) - only for first semester
