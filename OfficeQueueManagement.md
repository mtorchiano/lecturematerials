---
title: Office Queue Management Description
date: 2020-10-06
version: 2.0.1
---
<!-- pandoc -o OfficeQueueManagement.pdf OfficeQueueManagement.md -->

You need to develop a system to manage the queues for services open the public (e.g., post office, medical office). In the same office, various counters can handle different types of requests (e.g., shipping or accounts management).

The office consists of a set of counters that are usually identified by numbers (e.g. Counter 1, 2 etc.)

Each counter can handle different types of requests, which are defined at configuration phase. The definition consist of a tag name that identifies the request type and an estimate of the average time needed to process that request type (known as service time). A counter can serve more than one request type.

Customers come to the office because they have a need, corresponding to one of the request types that the office can to handle. When they get to the office they specify the request type and receive a ticket with a number. 
Ticket numbers are unique for the whole office. The numbers will be used to call citizens to the correct counter when their request can be served.

The system maintains different queues, one for each request type, so it is possible to know the number of people waiting for each request type. Every morning the queues are reset.

When an officer at a counter is ready, he tells the system to call the next citizen. Based on the counter id (and the requests it can serve) the system returns the ticket number that will be handled by that counter.

The next ticket to serve is selected with the following criterion: select the first number from the longest queue among those corresponding to the types the counter can manage. If two or more queues have the same length, the queue associated with request type having the lowest service time is selected.

If all the queues the counter can manage are empty, the system does nothing.

Selected tickets are considered served and removed from their queue (which is therefore shortened by one).


The system sends notifications concerning the length of the queues and the tickets called at the counters.  When a new ticket is issued, one queue changes, therefore, the system shows on the main display board a queue length has changed (and possibly notify all the interested customers).

Each time a new ticket is served the following actions must be performed:

- show on the main display board that a new ticket number is being served at a given counter;
- show on the main display board that the length of the queue associated with the type of served ticket has been shortened

The system should also provide an estimate of the waiting (minimum) time for the holder of a given ticket number.
The waiting time is evaluated as:

$$T_r = t_r \cdot \left( \frac{n_r}{\sum_i{ \frac{1}{k_i} \cdot s_{i,r}}} + \frac{1}{2}\right)$$

where:

- $t_r$ is the service time for request type $r$
- $n_r$ is the number of people in queue for request type $r$
- $k_i$ is the number of different types of requests served by counter $i$
- $s_{i,r}$ is an indicator variable equal to $1$ if counter $i$ can serve request $r$, $0$ otherwise.

As an example:

> John enters the post office to deposit money. There are two counters that can perform that service, one exclusively the other as an alternative to sending packages. There are 4 people waiting for the same service.
> 
> If we assume the service time is 5 min the expected waiting time would be
> 
> $$T_r = 5 \cdot \left( \frac{4}{1 +~^1/_2} + \frac{1}{2}\right) = 5 \cdot \left( \frac{8}{3} + \frac{1}{2}\right) = 15:50  $$

A manager in the office must be able to know how many customers have been served for each request type. In addition the system should provide the number of customers each counter has served, further divided by request type. Stats must be provided per day, week, month.
